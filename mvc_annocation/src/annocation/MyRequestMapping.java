package annocation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface MyRequestMapping {
	String value();

	String formbeans();
	
	String success();

	String fail();
	
	
}
