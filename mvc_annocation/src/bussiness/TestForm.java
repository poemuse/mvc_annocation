package bussiness;

import struts.form.ActionForm;

public class TestForm extends ActionForm {

	public TestForm() {
		// TODO Auto-generated constructor stub
	}

	private String name = "";
	private String pwd = "";
	private String age = "";

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

}
