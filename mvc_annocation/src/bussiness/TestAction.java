package bussiness;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import annocation.MyRequestMapping;
import struts.action.Action;
import struts.form.ActionForm;

public class TestAction implements Action {

	@MyRequestMapping(fail = "/view/test/fail.jsp", success = "/view/test/myjsp.jsp", value = "/test", formbeans = "bussiness.TestForm")
	public String execute(HttpServletRequest request,HttpServletResponse response, ActionForm form, Map<String, String> actionforward) {
		String url = "fail";
		TestForm testForm = (TestForm) form;
		if (testForm.getName().equals("wsj")) {
			url = "success";
		}

		return actionforward.get(url);
	}

}
