package struts.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bussiness.RouteAction;
import service.vo.MessageVO;
import struts.form.ActionForm;
import struts.form.FullForm;
import struts.form.XmlBean;

public class ActionServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获得请求头
		String path = this.getPath(request.getServletPath());
		// 从缓存中获取配置文件,配置文件由初始化的时候加载
		Map<String, XmlBean> map = (Map<String, XmlBean>) this.getServletContext().getAttribute("annocation");
		// 获取action都应的xml文件
		XmlBean xml = map.get(path);
		// 获取form类的路径
		String formclass = xml.getFormClass();
		// 通过反射，根据类的路径创建对象
		ActionForm form = FullForm.full(formclass, request);
		String actionType = xml.getActionType();
		Action action = null;
		String url = "";
		try {
			Class clazz = Class.forName(actionType);
			// 为什么可以这么做，是因为所有方法都写了默认的构造器
			action = (Action) clazz.newInstance();
			url = action.execute(request, response, form, xml.getActionForward());
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("严重：控制器异常！");
		}
		RequestDispatcher dis = request.getRequestDispatcher(url);
		dis.forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		this.doGet(request, response);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -3137831652369054389L;

	private String getPath(String servletpath) {
		return servletpath.split("\\.")[0];
	}
}
