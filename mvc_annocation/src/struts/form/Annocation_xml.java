package struts.form;

import java.io.File;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import annocation.MyRequestMapping;
import util.PackageUtil;

/**
 * 通过解析类将Annocation解析成xml
 * 
 * @author David_www.yigongke.com_QQ_519501574
 *
 */
public class Annocation_xml {

	public Annocation_xml() {

	}

	/**
	 * 将Annocation注解扫描成map映射
	 * 
	 * @param xmlpath
	 * @return
	 * @throws Exception
	 */
	public static Map<String, XmlBean> annocation_xml(String pkg) throws Exception {
		// Action也可能是多个
		Map<String, XmlBean> rmap = new HashMap<String, XmlBean>();

		List<String> paths = PackageUtil.getClassName(pkg);
		for (String string : paths) {
			Class clazz = Class.forName(string);
			// 查找class上面的注解
			if (clazz.isAnnotationPresent(MyRequestMapping.class)) {
				MyRequestMapping myRequestMapping = (MyRequestMapping) clazz.getAnnotation(MyRequestMapping.class);
				System.out.println("类上有@MyRequestMapping注解");
				Annocation_xml.setRMap(myRequestMapping, rmap, string);
				// rmap.put(path, action);
			}

			// 查找method上面的注解
			Method[] methods = clazz.getDeclaredMethods();
			for (Method method : methods) {
				if (method.isAnnotationPresent(MyRequestMapping.class)) {
					System.out.println(method.getName() + "方法上有@MyRequestMapping注解");
					MyRequestMapping myRequestMapping = (MyRequestMapping) method.getAnnotation(MyRequestMapping.class);
					Annocation_xml.setRMap(myRequestMapping, rmap, string);
				}
			}
		}

		return rmap;
	}

	private static Map<String, XmlBean> setRMap(MyRequestMapping myRequestMapping, Map<String, XmlBean> rmap,
			String type) {
		String name = "";
		String formclass = myRequestMapping.formbeans();
		String path = myRequestMapping.value();
		String success = myRequestMapping.success();
		String fail = myRequestMapping.fail();

		XmlBean action = new XmlBean();
		action.setBeanName(name);
		action.setFormClass(formclass);
		action.setActionClass(formclass);
		action.setPath(path);
		action.setActionType(type);
		Map<String, String> map = new HashMap<String, String>();
		map.put("success", success);
		map.put("fail", fail);
		action.setActionForward(map);
		rmap.put(path, action);
		return rmap;
	}
}
