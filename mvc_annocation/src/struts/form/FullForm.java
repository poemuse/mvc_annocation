package struts.form;

import java.lang.reflect.Field;

import javax.servlet.http.HttpServletRequest;

public class FullForm {
	public FullForm() {

	}

	public static ActionForm full(String formpath, HttpServletRequest request) {
		ActionForm form = null;
		// 使用类反射，类反射一定会出异常
		try {
			Class clazz = Class.forName(formpath);
			// 为什么可以这么做：因为每一个ActionForm都写过一个容器。
			form = (ActionForm) clazz.newInstance();
			// 通过类反射来获得类的所有属性
			Field[] fiedd_ar = clazz.getDeclaredFields();
			for (Field f : fiedd_ar) {
				//设置可以介入操作,询问式的，这是最特殊的。
				f.setAccessible(true);
				//所以这个要求前后台的值是一样的
				f.set(form, request.getParameter(f.getName()));
				f.setAccessible(false);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("严重：Form 装载失败！.....");
		}
		return form;
	}
}
