package struts.form;

import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ActionListener implements ServletContextListener {

	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("信息：系统已经注销");

	}

	public void contextInitialized(ServletContextEvent arg0) {
		ServletContext context = arg0.getServletContext();
		String xmlpath = context.getInitParameter("struts-config");
		String tomcatpath = context.getRealPath("\\");
		try {
			Map<String, XmlBean> map = Annocation_xml.annocation_xml("bussiness");
			// 加载xml到系统缓存中
			context.setAttribute("annocation", map);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("信息：系统已经加载完成！");
	}

}
